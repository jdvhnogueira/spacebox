# SpaceBox

Project developed in HTML, CSS and JS, using VanillaJS for Basement.

## Getting Started

Clone the repository with git and execute in linux server or linux system.

### Prerequisites

This projects needs Linux System.

### Installing

To Running use NPM

```
apt install npm
```

Install HTTP-Server

```
sudo  npm install http-server -g
```

Execute HTTP-Server

```
http-server
```

Access your browser and entering in this page

```
http://localhost:8080/
```

or

```
http://127.0.0.1:8080
```

## Built With

* [VanillaJS](http://vanilla-js.com/) - The Javascript powerup used

## Authors

* **João Danilo Ventura**
