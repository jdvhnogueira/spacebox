// Out Card Elements
const app = document.getElementById('root');
const container = document.createElement('div');
const info_card = document.createElement('div');
var card_contents = document.createElement('div');

// Create structures
container.setAttribute('class', 'container center');
info_card.setAttribute('class', 'info-card');
app.appendChild(container);
container.appendChild(info_card);
card_contents.setAttribute('class','card-content center');
info_card.appendChild(card_contents);

// Read API JSON
var request = new XMLHttpRequest();
request.open('GET', 'dados.json', true);
request.onload = function () {

  // Begin accessing JSON data here
  var data = JSON.parse(this.response);
  if (request.status >= 200 && request.status < 400) {
    data.forEach(user => {
      // Card Info Basement
      const card = document.createElement('div');
      const content = document.createElement('div');
      const circle = document.createElement('div');
      const user_id = document.createElement('div');
      const img = document.createElement('img');
      const content_text = document.createElement('div');
      const br = document.createElement('br');
      const name = document.createElement('b');
      const occupation = document.createElement('i');

      // Card Result Basement
      const info_content = document.createElement('div');
      const info_circle = document.createElement('div');
      const info_img = document.createElement('img');
      const info_content_text = document.createElement('div');
      const info_name = document.createElement('b');
      const full_name = document.createElement('p');
      const info_occupation = document.createElement('b');
      const full_occupation = document.createElement('p');
      const info_age = document.createElement('b');
      const full_age = document.createElement('p');

      // Card Info Attributes
      card.setAttribute('class', 'card');
      card.addEventListener('click', view_user);
      content.setAttribute('class', 'card-content center');
      circle.setAttribute('class', 'circle center');
      user_id.setAttribute('class', 'number_id center');
      user_id.textContent = user.id
      img.src = `images/${user.foto}`;
      img.setAttribute('class', 'avatar');
      content_text.setAttribute('class', 'card-text');
      name.textContent = user.nome;
      occupation.textContent = user.cargo;

      // Card Result Attributes
      info_content.setAttribute('class', 'card-content center');
      info_circle.setAttribute('class', 'circle center');
      info_img.src = `images/${user.foto}`;
      img.setAttribute('class', 'avatar');
      info_content_text.setAttribute('class', 'card-text-info');
      info_name.textContent = user.nome;
      full_name.textContent = 'Nome: ';
      info_occupation.textContent = user.cargo;
      full_occupation.textContent = 'Ocupação: ';
      info_age.textContent = user.idade;
      full_age.textContent = 'Idade: ';

      // Card Info Construction
      container.appendChild(card);
      card.appendChild(content);
      content.appendChild(circle);
      circle.appendChild(img);
      content.appendChild(content_text);
      content_text.appendChild(name);
      content_text.appendChild(br);
      content_text.appendChild(occupation);
      content.appendChild(user_id);

      // Card Result Construction
      info_content.appendChild(info_circle);
      info_circle.appendChild(info_img);
      info_content.appendChild(info_content_text);
      info_content_text.appendChild(full_name);
      full_name.appendChild(info_name);
      info_content_text.appendChild(full_occupation);
      full_occupation.appendChild(info_occupation);
      info_content_text.appendChild(full_age);
      full_age.appendChild(info_age);

      // Click Function Event
      function view_user(event) {
        var select_user = info_content.cloneNode(true);
        remove_class = document.getElementById('fill-background');
        if (remove_class != null) {
          remove_class.setAttribute('class', 'card');
          remove_class.setAttribute('id', '');
        }
        card.setAttribute('class', 'card fill-background');
        card.setAttribute('id', 'fill-background');
        info_card.replaceChild(select_user,card_contents);
        card_contents = select_user;
      }
    });
  } else {
    const errorMessage = document.createElement('marquee');
    errorMessage.textContent = `Algo deu errado!`;
    app.appendChild(errorMessage);
  }
}

request.send();
